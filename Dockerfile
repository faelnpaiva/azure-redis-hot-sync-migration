FROM ubuntu

WORKDIR /opt/

COPY ./entrypoint.sh .

RUN apt-get update && \
    apt-get install -y wget redis-tools && \
    wget https://github.com/stickermule/rump/releases/download/1.0.0/rump-1.0.0-linux-amd64 && \
    mv rump-1.0.0-linux-amd64 /usr/local/bin/rump && \
    chmod +x /usr/local/bin/rump && \
    chmod +x entrypoint.sh 
 
ENV PATH=${PATH}:/usr/local/bin/rump

ENTRYPOINT ./entrypoint.sh
